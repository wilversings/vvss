package biblioteca.control;


import biblioteca.model.Book;
import biblioteca.repository.repoInterfaces.IBooksRepo;
import biblioteca.util.Validator;

import java.util.ArrayList;
import java.util.List;

public class LibraryCtrl {

	private IBooksRepo cr;
	
	public LibraryCtrl(IBooksRepo cr){
		this.cr = cr;
	}
	
	public void addBook(String title, ArrayList<String> referenti, String releaseYear, ArrayList<String> keywords) throws Exception{
		Book c = new Book(title, referenti, releaseYear, keywords);
		Validator.validateCarte(c);
		cr.addBook(c);
	}
	
	public List<Book> searchBook(String autor) throws Exception{
		Validator.isStringOK(autor);
		return cr.findBook(autor);
	}
	
	public List<Book> getBooks() throws Exception{
		return cr.getBooks();
	}
	
	public List<Book> getBooksSortedFromYear(String an) throws Exception{
		if(!Validator.isNumber(an))
			throw new Exception("Nu e numar!");
		return cr.getBooksSortedFromYear(an);
	}
	
	
}
