package biblioteca.model;


import java.util.ArrayList;
import java.util.List;

public class Book {
	
	private String title;
	private List<String> referenti;
	private String releaseYear;
	private List<String> keywords;
	
	public Book() {
		title = "";
		referenti = new ArrayList<String>();
		releaseYear = "";
		keywords = new ArrayList<String>();
	}
	public Book(String title, ArrayList<String> referenti, String releaseYear, ArrayList<String> keywords) {
		this.title = title;
		this.referenti = referenti;
		this.releaseYear = releaseYear;
		this.keywords = keywords;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public List<String> getReferenti() {
		return referenti;
	}

	public void setReferenti(List<String> ref) {
		this.referenti = ref;
	}

	public String getReleaseYear() {
		return releaseYear;
	}

	public void setReleaseYear(String releaseYear) {
		this.releaseYear = releaseYear;
	}

	public List<String> getKeywords() {
		return keywords;
	}

	public void setKeywords(List<String> keywords) {
		this.keywords = keywords;
	}

	public void addKeyword(String cuvant){
		keywords.add(cuvant);
	}
	
	public void adaugaReferent(String ref){
		referenti.add(ref);
	}
	
	public boolean cautaDupaCuvinteCheie(List<String> cuvinte){
		for(String c: keywords){
			for(String cuv:cuvinte){
				if(c.equals(cuv))
					return true;
			}
		}
		return false;
	}
	 
	@Override
	public String toString(){
		String ref = "";
		String cuvCheie = "";
		
		for(int i=0;i<referenti.size();i++){
			if(i==referenti.size()-1)
				ref+=referenti.get(i);
			else
				ref+=referenti.get(i)+",";
		}
		
		for(int i = 0; i< keywords.size(); i++){
			if(i== keywords.size()-1)
				cuvCheie+= keywords.get(i);
			else
				cuvCheie+= keywords.get(i)+",";
		}
		
		return title +";"+ref+";"+ releaseYear +";"+cuvCheie;
	}
	
	public static Book getBookFromString(String carte){
		Book c = new Book();
		String [] atr = carte.split(";");
		String [] referenti = atr[1].split(",");
		String [] keyword = atr[4].split(",");
		
		c.title =atr[0];
		for(String s:referenti){
			c.adaugaReferent(s);
		}
		c.releaseYear = atr[2];
		for(String s: keyword){
			c.addKeyword(s);
		}
		
		return c;
	}
	
}
