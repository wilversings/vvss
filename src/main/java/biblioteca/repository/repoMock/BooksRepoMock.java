package biblioteca.repository.repoMock;


import biblioteca.model.Book;
import biblioteca.repository.repo.BooksRepo;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class BooksRepoMock extends BooksRepo {

	private List<Book> books;

	public BooksRepoMock(){
		books = new ArrayList<Book>();
		
		books.add(Book.getBookFromString("Povesti;Mihai Eminescu,Ion Caragiale,Ion Creanga;1973;Corint;povesti,povestiri"));
		books.add(Book.getBookFromString("Poezii;Sadoveanu;1973;Corint;poezii"));
		books.add(Book.getBookFromString("Enigma Otiliei;George Calinescu;1948;Litera;enigma,otilia"));
		books.add(Book.getBookFromString("Dale carnavalului;Caragiale Ion;1948;Litera;caragiale,carnaval"));
		books.add(Book.getBookFromString("Intampinarea crailor;Mateiu Caragiale;1948;Litera;mateiu,crailor"));
		books.add(Book.getBookFromString("Test;Calinescu,Tetica;1992;Pipa;am,casa"));

	}
	
	@Override
	public void addBook(Book c) {
		books.add(c);
	}
	

	@Override
	public List<Book> getBooks() {
		return books;
	}

	@Override
	public void editBook(Book nou, Book vechi) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void deleteBook(Book c) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public List<Book> getBooksSortedFromYear(String an) {
		List<Book> lc = getBooks();
		List<Book> lca = new ArrayList<Book>();
		for(Book c:lc){
			if(c.getReleaseYear().equals(an) == false){
				lca.add(c);
			}
		}
		
		Collections.sort(lca,new Comparator<Book>(){

			@Override
			public int compare(Book a, Book b) {
				if(a.getTitle().compareTo(b.getTitle())==0){
					return a.getReferenti().get(0).compareTo(b.getReferenti().get(0));
				}
				
				return a.getTitle().compareTo(b.getTitle());
			}
		
		});
		
		return lca;
	}

}
