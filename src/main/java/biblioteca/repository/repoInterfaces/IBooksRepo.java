package biblioteca.repository.repoInterfaces;


import biblioteca.model.Book;

import java.util.List;

public interface IBooksRepo {
	void addBook(Book c);
	void editBook(Book nou, Book vechi);
	void deleteBook(Book c);
	List<Book> findBook(String ref);
	List<Book> getBooks();
	List<Book> getBooksSortedFromYear(String an);
}
