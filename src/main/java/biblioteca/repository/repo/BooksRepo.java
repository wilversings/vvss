package biblioteca.repository.repo;


import biblioteca.model.Book;
import biblioteca.repository.repoInterfaces.IBooksRepo;

import java.io.*;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class BooksRepo implements IBooksRepo {
	
	private String file = "cartiBD.txt";
	
	public BooksRepo(){
		URL location = BooksRepo.class.getProtectionDomain().getCodeSource().getLocation();
        System.out.println(location.getFile());
	}

	public BooksRepo(String filePath) {
		this();
		file = filePath;
	}
	
	@Override
	public void addBook(Book c) {
		BufferedWriter bw = null;
		try {
			bw = new BufferedWriter(new FileWriter(file,true));
			bw.write(c.toString());
			bw.newLine();
			
			bw.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Override
	public List<Book> getBooks() {
		List<Book> lc = new ArrayList<Book>();
		BufferedReader br = null;
		try {
			br = new BufferedReader(new FileReader(file));
			String line = null;
			while((line=br.readLine())!=null){
				lc.add(Book.getBookFromString(line));
			}
			
			br.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		return lc;
	}

	@Override
	public void editBook(Book nou, Book vechi) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void deleteBook(Book c) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public List<Book> findBook(String ref) {
		List<Book> carti = getBooks();
		List<Book> cartiGasite = new ArrayList<Book>();
		int i=0;
		while (i < carti.size()) { // Req02_Cond02
			boolean flag = false;
			List<String> lref = carti.get(i).getReferenti();
			int j = 0;
			while(j < lref.size()){ // Req02_Cond05
				if(lref.get(j).toLowerCase().contains(ref.toLowerCase())){ // Req02_Cond08
					flag = true;
					break;
				}
				else
					j++;
			}
			if(flag == true){ // Req02_Cond06
				cartiGasite.add(carti.get(i));
			}
			i++;
		}
		return cartiGasite;
	}

	@Override
	public List<Book> getBooksSortedFromYear(String an) {
		List<Book> lc = getBooks();
		List<Book> lca = new ArrayList<Book>();
		for(Book c:lc){
			if(c.getReleaseYear().equals(an) == false){
				lca.add(c);
			}
		}
		
		Collections.sort(lca,new Comparator<Book>(){

			@Override
			public int compare(Book a, Book b) {
				if(a.getReleaseYear().compareTo(b.getReleaseYear())==0){
					return a.getTitle().compareTo(b.getTitle());
				}
				
				return a.getTitle().compareTo(b.getTitle());
			}
		
		});
		
		return lca;
	}

}
