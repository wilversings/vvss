package biblioteca.util;

import biblioteca.model.Book;

public class Validator {
	
	public static boolean isStringOK(String s) throws Exception{
		boolean flag = s.matches("[a-zA-Z]+");
		return flag;
	}
	
	public static void validateCarte(Book c)throws Exception{
		if(c.getKeywords()==null){
			throw new Exception("Keywords is null!");
		}
		if(c.getReferenti()==null){
			throw new Exception("Lista autori vida!");
		}
		if(!isOKString(c.getTitle()))
			throw new Exception("Title invalid!");
		for(String s:c.getReferenti()){
			if(!isOKString(s))
				throw new Exception("Author invalid!");
		}
		for(String s:c.getKeywords()){
			if(!isOKString(s))
				throw new Exception("Cuvant cheie invalid!");
		}
		if(!Validator.isNumber(c.getReleaseYear()))
			throw new Exception("Release year invalid!");
	}
	
	public static boolean isNumber(String s){
		return s.matches("[0-9]+");
	}
	
	public static boolean isOKString(String s){
		String []t = s.split(" ");
		if(t.length==2){
			boolean ok1 = t[0].matches("[a-zA-Z]+");
			boolean ok2 = t[1].matches("[a-zA-Z]+");
			if(ok1==ok2 && ok1==true){
				return true;
			}
			return false;
		}
		return s.matches("[a-zA-Z]+");
	}
	
}
