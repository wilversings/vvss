package biblioteca.view;


import biblioteca.control.LibraryCtrl;
import biblioteca.model.Book;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;

public class Console {

	private BufferedReader console;
	LibraryCtrl bc;
	
	public Console(LibraryCtrl bc){
		this.bc=bc;
	}
	
	public void execute() throws IOException {
		
		console = new BufferedReader(new InputStreamReader(System.in));
		
		int opt = -1;
		while(opt!=0){
			
			switch(opt){
				case 1:
					add();
					break;
				case 2:
					searchBooksByAuthor();
					break;
				case 3:
					showBooksSortedByYear();
					break;
				case 4:
					displayAllBooks();
					break;
			}
		
			printMenu();
			String line;
			do{
				System.out.println("Introduceti un nr:");
				line=console.readLine();
			}while(!line.matches("[0-4]"));
			opt=Integer.parseInt(line);
		}
	}
	
	public void printMenu(){
		System.out.println("\n\n\n");
		System.out.println("Evidenta cartilor dintr-o biblioteca");
		System.out.println("     1. Adaugarea unei noi carti");
		System.out.println("     2. Cautarea cartilor scrise de un anumit autor");
		System.out.println("     3. Afisarea cartilor din biblioteca care au aparut intr-un anumit an, ordonate alfabetic dupa titlu si autori");
		System.out.println("     4. Afisarea toturor cartilor");
		System.out.println("     0. Exit");
	}
	
	public void add(){

		String title, releaseYear;
		ArrayList<String> referenti = new ArrayList<String>(), keywords = new ArrayList<String>();

		try{
			System.out.println("\n\n\n");
			
			System.out.println("Titlu:");
			title = console.readLine();
			
			String line;
			do{
				System.out.println("An aparitie:");
				line=console.readLine();
			}while(!line.matches("[10-9]+"));
			releaseYear = line;
			
			do{
				System.out.println("Nr. de referent:");
				line=console.readLine();
			}while(!line.matches("[1-9]+"));
			int nrReferenti= Integer.parseInt(line);
			for(int i=1;i<=nrReferenti;i++){
				System.out.println("Autor "+i+": ");
				referenti.add(console.readLine());
			}
			
			do{
				System.out.println("Nr. de cuvinte cheie:");
				line=console.readLine();
			}while(!line.matches("[1-9]+"));
			int nrCuv= Integer.parseInt(line);
			for(int i=1;i<=nrCuv;i++){
				System.out.println("Cuvant "+i+": ");
				keywords.add(console.readLine());
			}
			
			bc.addBook(title, referenti, releaseYear, keywords);
			
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	public void displayAllBooks(){
		System.out.println("\n\n\n");
		try {
			for(Book c:bc.getBooks())
				System.out.println(c);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void searchBooksByAuthor(){
	
		System.out.println("\n\n\n");
		System.out.println("Autor:");
		try {
			for(Book c:bc.searchBook(console.readLine())){
				System.out.println(c);
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void showBooksSortedByYear(){
		System.out.println("\n\n\n");
		try{
			String line;
			do{
				System.out.println("An aparitie:");
				line=console.readLine();
			}while(!line.matches("[10-9]+"));
			for(Book c:bc.getBooksSortedFromYear(line)){
				System.out.println(c);
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		
	}
}
