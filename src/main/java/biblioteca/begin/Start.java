package biblioteca.begin;

import biblioteca.control.LibraryCtrl;
import biblioteca.repository.repo.BooksRepo;
import biblioteca.repository.repoInterfaces.IBooksRepo;
import biblioteca.repository.repoMock.BooksRepoMock;
import biblioteca.view.Console;

import java.io.IOException;

//functionalitati
//i.	 adaugarea unei noi carti (titlu, autori, an aparitie, editura, cuvinte cheie);
//ii.	 cautarea cartilor scrise de un anumit autor (sau parti din numele autorului);
//iii.	 afisarea cartilor din biblioteca care au aparut intr-un anumit an, ordonate alfabetic dupa titlu si autori.



public class Start {
	
	public static void main(String[] args) {
		IBooksRepo cr = new BooksRepo();
		LibraryCtrl bc = new LibraryCtrl(cr);
		Console c = new Console(bc);
		try {
			c.execute();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
//	IBooksRepo cr = new BooksRepo();
//	LibraryCtrl bc = new LibraryCtrl(cr);
//	
//	Book c = new Book();
//	bc = new LibraryCtrl(cr);
//	c = new Book();
//	
//	List<String> autori = new ArrayList<String>();
//	autori.add("Mateiu Caragiale");
//	
//	List<String> cuvinteCheie = new ArrayList<String>();
//	cuvinteCheie.add("mateiu");
//	cuvinteCheie.add("crailor");
//	
//	c.setTitle("Intampinarea crailor");
//	c.setAutori(autori);
//	c.setReleaseYear("1948");
//	c.setEditura("Litera");
//	c.setKeywords(cuvinteCheie);
//	
//	
//	try {
//		for(Book ca:bc.getBooksSortedFromYear("1948"))
//			System.out.println(ca);
//	} catch (Exception e) {
//		// TODO Auto-generated catch block
//		e.printStackTrace();
//	}
//
//	
////	try {
////		bc.addBook(c);
////	} catch (Exception e) {
////		// TODO Auto-generated catch block
////		e.printStackTrace();
////	}
	
}
