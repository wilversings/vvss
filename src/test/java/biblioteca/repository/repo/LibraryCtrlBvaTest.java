package biblioteca.repository.repo;

import biblioteca.control.LibraryCtrl;
import biblioteca.model.Book;
import biblioteca.repository.repoInterfaces.IBooksRepo;
import biblioteca.repository.repoMock.BooksRepoMock;
import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;

import static org.junit.Assert.*;

public class LibraryCtrlBvaTest {


    IBooksRepo booksRepo = new BooksRepoMock();
    LibraryCtrl subject = new LibraryCtrl(booksRepo);
    ArrayList<String> referenti = new ArrayList<String>();
    ArrayList<String> keywords = new ArrayList<String>();

    public LibraryCtrlBvaTest() {
        referenti.add("ReferintA");
        referenti.add("ReferintB");
        referenti.add("ReferintC");

        keywords.add("KeywordD");
        keywords.add("KeywordD");
    }

    @Test
    public void testCase1() {
        try {
            subject.addBook("", referenti, "2010", keywords);
            Assert.fail();
        }
        catch(Exception ex) {
            Assert.assertEquals("Title invalid!", ex.getMessage());
        }
    }
    @Test
    public void testCase3() throws Exception {
        subject.addBook("M", referenti, "2010", keywords);

        for(Book book: booksRepo.getBooks()) {
            if (book.getTitle().equals("M")) {
                return;
            }
        }
        Assert.fail();
    }
    @Test
    public void testCase4() throws Exception {
        String str = "";
        for (int i = 0; i < 256; ++i) {
            str += "A";
        }

        subject.addBook(str, referenti, "2010", keywords);

        for(Book book: booksRepo.getBooks()) {
            if (book.getTitle().equals(str)) {
                return;
            }
        }
        Assert.fail();

    }

}
