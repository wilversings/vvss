package biblioteca.repository.repo;

import biblioteca.control.LibraryCtrl;
import biblioteca.model.Book;
import biblioteca.repository.repoInterfaces.IBooksRepo;
import biblioteca.repository.repoMock.BooksRepoMock;
import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;


public class BooksRepoWbtTest {

    @Test
    public void tc0() {

        BooksRepo subject = new BooksRepo("testBD1.txt");
        List<Book> books = subject.findBook("Sadoveanu");

        Assert.assertEquals(1, books.size());
        Assert.assertEquals("Poezii", books.get(0).getTitle());

    }

    @Test
    public void tc1() {

        BooksRepo subject = new BooksRepo("testBD1.txt");
        List<Book> books = subject.findBook("Ion Creanga");

        Assert.assertEquals(2, books.size());
        Assert.assertEquals("Povesti", books.get(0).getTitle());
        Assert.assertEquals("Povesti", books.get(1).getTitle());

    }

}
