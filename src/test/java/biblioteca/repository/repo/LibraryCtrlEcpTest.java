package biblioteca.repository.repo;

import biblioteca.control.LibraryCtrl;
import biblioteca.model.Book;
import biblioteca.repository.repoInterfaces.IBooksRepo;
import biblioteca.repository.repoMock.BooksRepoMock;
import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;

import static org.junit.Assert.*;

public class LibraryCtrlEcpTest {

    IBooksRepo booksRepo = new BooksRepoMock();
    LibraryCtrl subject = new LibraryCtrl(booksRepo);
    ArrayList<String> referenti = new ArrayList<String>();
    ArrayList<String> keywords = new ArrayList<String>();

    public LibraryCtrlEcpTest() {
        referenti.add("ReferintA");
        referenti.add("ReferintB");
        referenti.add("ReferintC");

        keywords.add("KeywordD");
        keywords.add("KeywordD");
    }

    @Test
    public void testCase1() throws Exception {

        subject.addBook("BookTitle", referenti, "2010", keywords);

        for(Book book: booksRepo.getBooks()) {
            if (book.getTitle().equals("BookTitle")) {
                return;
            }
        }
        Assert.fail();

    }

    @Test
    public void testCase2() {

        try {
            subject.addBook("BookTitle", referenti, "doua mii", keywords);
            Assert.fail();
        } catch (Exception e) {
            Assert.assertEquals("Release year invalid!", e.getMessage());
        }

    }

    @Test
    public void testCase3 () {
        try {
            subject.addBook("BookTitle3", referenti, "2011", keywords);
            Assert.fail();
        } catch (Exception e) {
            Assert.assertEquals("Title invalid!", e.getMessage());
        }
    }

    @Test
    public void testCase4 () {
        try {
            subject.addBook("BookTitle", referenti, "2011", null);
            Assert.fail();
        } catch (Exception e) {
            Assert.assertEquals("Keywords is null!", e.getMessage());
        }
    }

}